/*==============================================*/
/*			Author: Berend de Groot				*/
/*			Mouse Over Functions 				*/
/*			Versie: 2.0							*/
/*==============================================*/

$(document).ready(function(){  

	$(".menu > div").mouseover(function(e) {
		$("#home").removeClass("mouse");
		$("#product").removeClass("mouse");
		$("#verkrijg").removeClass("mouse");
		$("#meer").removeClass("mouse");
		$("#belofte").removeClass("mouse");
		$("#contact").removeClass("mouse");
		switch(e.target.id){
			case "home": 
				if($(this).hasClass('hclick')) {
					$("#home").removeClass("mouse");
				}
				else {
					$("#home").addClass("mouse");
				}
            break; 
			
			case "product":
				if($(this).hasClass('pclick')) {
					$("#home").removeClass("mouse");
				}
				else {
					$("#product").addClass("mouse");
				}	
            break;
			
			case "p1":
				if($(this).hasClass('pclick')) {
					$("#home").removeClass("mouse");
				}
				else {
					$("#p1").addClass("mouse");
				}
			  break;
				
			case "p2":
				if($(this).hasClass('pclick')) {
					$("#home").removeClass("mouse");
				}
				else {
					$("#p2").addClass("mouse");
				}
			  break;
			
			case "verkrijg": 
				if($(this).hasClass('vclick')) {
					$("#verkrijg").removeClass("mouse");
				}
				else {
					$("#verkrijg").addClass("mouse");
				}
            break; 
			
			case "meer": 
           		if($(this).hasClass('mclick')) {
					$("#meer").removeClass("mouse");
				}
				else {
					$("#meer").addClass("mouse");
				}
            break; 
			
			case "belofte": 
				if($(this).hasClass('bclick')) {
					$("#belofte").removeClass("mouse");
				}
				else {
					$("#belofte").addClass("mouse");
				}
            break; 
			
			case "contact": 
				if($(this).hasClass('cclick')) {
					$("#contact").removeClass("mouse");
				}
				else {
					$("#contact").addClass("mouse");
				}
            break; 
        }  
        return false;  
    }); 
}); 