/*==============================================*/
/*			Author: Berend de Groot				*/
/*			Click Funtions						*/
/*			Versie: 2.0							*/
/*==============================================*/
$(document).ready(function(){  

	$(".menu > div").click(function(e){  
		$("#home").removeClass("hclick");
		$("#product").removeClass("pclick");
		$("#verkrijg").removeClass("vclick");
		$("#meer").removeClass("mclick");
		$("#belofte").removeClass("bclick");
		$("#contact").removeClass("cclick");
		$("#p1").removeClass("p1click");
		$("#p2").removeClass("p2click");
		switch(e.target.id){
			case "home": 
            	$("#home").addClass("hclick");
            break; 
			case "product": 
            	$("#product").addClass("pclick");
            break; 
			case "p1": 
            	$("#p1").addClass("p1click");
            break; 
			case "p2": 
            	$("#p2").addClass("p2click");
            break; 
				case "verkrijg": 
            	$("#verkrijg").addClass("vclick");
            break; 
			case "meer": 
            	$("#meer").addClass("mclick");
            break; 
			case "belofte": 
            	$("#belofte").addClass("bclick");
            break; 
			case "contact": 
            	$("#contact").addClass("cclick");
            break; 
        }  
        return false;  
    }); 
});  
