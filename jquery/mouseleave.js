/*==============================================*/
/*			Author: Berend de Groot				*/
/*			Mouse Leave Functions 				*/
/*			Versie: 2.0							*/
/*==============================================*/
$(document).ready(function(){  
	
		$(".menu > div").mouseleave(function(e) {
			$("#home").removeClass("mouse");
			$("#product").removeClass("mouse");
			$("#p1").removeClass("mouse");
			$("#p2").removeClass("mouse");
			$("#verkrijg").removeClass("mouse");
			$("#meer").removeClass("mouse");
			$("#belofte").removeClass("mouse");
			$("#contact").removeClass("mouse");  
		}); 
	
	}); 