/*==============================================*/
/*			Author: Berend de Groot				*/
/*			Versie: 1.2							*/
/*==============================================*/
$(document).ready(function(){  

	$(".menu > div").click(function(e){  
		$("#home").removeClass("hmouse");
		$("#product").removeClass("pmouse");
		$("#verkrijg").removeClass("vmouse");
		$("#meer").removeClass("mmouse");
		$("#belofte").removeClass("bmouse");
		$("#contact").removeClass("cmouse");
		$("#p1").removeClass("cmouse");
		$("#p2").removeClass("cmouse");
		switch(e.target.id){
			case "home": 
            	$("#home").addClass("hmouse");
            break; 
			case "product": 
            	$("#product").addClass("pmouse");
            break; 
			case "p1": 
            	$("#p1").addClass("pmouse");
            break;
			case "p2": 
            	$("#p2").addClass("pmouse");
            break;
				case "verkrijg": 
            	$("#verkrijg").addClass("vmouse");
            break; 
			case "meer": 
            	$("#meer").addClass("mmouse");
            break; 
			case "belofte": 
            	$("#belofte").addClass("bmouse");
            break; 
			case "contact": 
            	$("#contact").addClass("cmouse");
            break; 
        }  
        return false;  
    }); 
});  
