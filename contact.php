<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en'> 
<head>
	<title>FlevoLand Flowers</title>
	<!-- Meta content -->
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /> 
	<meta name='description' content='Flevoland Flowers rozen' />
	<meta name='copyright' content='b3r3nd.nl' />
	<meta name='author' content='Berend de Groot' />
	<meta name='language' content='en-EN, English' />
	<link rel='shortcut icon' href='pics/icon.gif' />

	<!-- jQuery Functions -->
	<script src='jquery/lib.js'></script>
	<script src='jquery/tabclick.js'></script>
	<script src='jquery/mouseover.js'></script>
	<script src='jquery/mouseleave.js'></script>
	<script src='jquery/loadpages.js'></script>
	
	<link rel='stylesheet' type='text/css' href='css/cindex.css' />
</head>
<body>
<center>
	<div id='header'><a href='tour/tour.html'><div id='ntour'>Tour</div></a>
	<div id='tabs' class='menu'>
		<a href='home.html'><div id='home' class='tabs' >Home &nbsp;</div></a>
		<a href='rednaomi.html'><div id='p1' class='tabs' >Red Naomi &nbsp;</div></a>
		<a href='grandprix.html'><div id='p2' class='tabs' >Grand Prix &nbsp;</div></a>
		<a href='verkrijg.html'><div id='verkrijg' class='tabs' >&nbsp;Verkrijgbaarheid &nbsp;</div></a>
		<a href='meer.html'><div id='meer' class='tabs' >&nbsp;Meer Over FF &nbsp;</div></a>
		<a href='belofte.html'><div id='belofte' class='tabs' >Belofte &nbsp;</div></a>
		<a href='contact.php'><div id='contact' class='tabs' >Contact &nbsp;</div></a>
	</div>
</div>
	<div id='ppage'>
<p>Contact</br>
</br>
Onze adres gegevens zijn:</br><br>
Flevoland Flowers</br>
Weteringweg 11</br>
8315 RL Luttelgeest</br>
Tel.  0527 - 68 10 44 </br>
Fax: 0527 - 68 58 14</br>
<a href='mailto:info@flevolandflowers.nl'>Info@flevolandflowers.nl</a></br>
</br>
Heeft u een vraag, mail hem ons. Ook voor het aanvragen van het dagelijkse aanvoer overzicht kunt u ons mailen.</br>
Of maak gebruik van de volgende tool:<br>
</p>
					<?php
 

$mail_ontv = 'info@flevolandflowers.nl'; 


if ($_SERVER['REQUEST_METHOD'] == 'POST')
{

    if (!preg_match('/[ a-zA-Z-]$/', $_POST['naam']))
        $naam_fout = 1;

    if (function_exists('filter_var') && !filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL))
            $email_fout = 1;

    if (!empty($_SESSION['antiflood']))
    {
        $seconde = 900; 
        $tijd = time() - $_SESSION['antiflood'];
        if($tijd < $seconde)
            $antiflood = 1;
    }
}


if (($_SERVER['REQUEST_METHOD'] == 'POST' && (!empty($antiflood) || empty($_POST['naam']) || !empty($naam_fout) || empty($_POST['mail']) || !empty($email_fout) || empty($_POST['bericht']) || empty($_POST['onderwerp']))) || $_SERVER['REQUEST_METHOD'] == 'GET')
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        if (!empty($naam_fout))
            echo '<p>Uw naam mag alleen letters bevatten.</p>';
        elseif (!empty($email_fout))
            echo '<p>Uw e-mailadres is niet juist.</p>';
        elseif (!empty($antiflood))
            echo '<p>U mag slechts &eacute;&eacute;n bericht per ' . $seconde . ' seconde versturen.</p>';
        else
            echo '<p>U bent uw naam, e-mailadres, onderwerp of bericht vergeten in te vullen.</p>';
    }
        

  echo '<form method="post" action="' . $_SERVER['REQUEST_URI'] . '" />
	<center><table border="0">
		<tr>
			<td>
				<label for="naam">Naam:</label>
			</td>
			<td>
				<input size="47" type="text" id="naam" name="naam" value="' . (isset($_POST['naam']) ? htmlspecialchars($_POST['naam']) : '') . '" /><br />
			</td>
		</tr>
		<tr>
			<td>
				<label for="mail">E-mailadres:</label>
			</td>
			<td>
				<input size="47" type="text" id="mail" name="mail" value="' . (isset($_POST['mail']) ? htmlspecialchars($_POST['mail']) : '') . '" /><br />
			</td>
		</tr>
		<tr>
			<td>
				<label for="onderwerp">Onderwerp:</label>
			</td>
			<td>
				<input size="47" type="text" id="onderwerp" name="onderwerp" value="' . (isset($_POST['onderwerp']) ? htmlspecialchars($_POST['onderwerp']) : '') . '" /><br />
			</td>
		</tr>
		<tr>
			<td>
				<label for="bericht">Bericht:</label>
			</td>
			<td>
		<textarea id="bericht" name="bericht" rows="8" style="width: 300px;height:200px;">' . (isset($_POST['bericht']) ? htmlspecialchars($_POST['bericht']) : '') . '</textarea><br />
			</td>
		</tr>
		</table>
      <input type="submit" name="submit" value=" Versturen " />
  </p>
  </form>';
}

else
{      

  $datum = date('d/m/Y H:i:s');
    
  $inhoud_mail = "===================================================\n";
  $inhoud_mail .= "Ingevulde contact formulier " . $_SERVER['HTTP_HOST'] . "\n";
  $inhoud_mail .= "===================================================\n\n";
  
  $inhoud_mail .= "Naam: " . htmlspecialchars($_POST['naam']) . "\n";
  $inhoud_mail .= "E-mail adres: " . htmlspecialchars($_POST['mail']) . "\n";
  $inhoud_mail .= "Bericht:\n";
  $inhoud_mail .= htmlspecialchars($_POST['bericht']) . "\n\n";
    
  $inhoud_mail .= "Verstuurd op " . $datum . " via het IP adres " . $_SERVER['REMOTE_ADDR'] . "\n\n";
    
  $inhoud_mail .= "===================================================\n\n";
  

  
  $headers = 'From: ' . htmlspecialchars($_POST['naam']) . ' <' . $_POST['mail'] . '>';
  
  $headers = stripslashes($headers);
  $headers = str_replace('\n', '', $headers); 
  $headers = str_replace('\r', '', $headers); 
  $headers = str_replace("\"", "\\\"", str_replace("\\", "\\\\", $headers)); 
  
  $_POST['onderwerp'] = str_replace('\n', '', $_POST['onderwerp']); 
  $_POST['onderwerp'] = str_replace('\r', '', $_POST['onderwerp']); 
  $_POST['onderwerp'] = str_replace("\"", "\\\"", str_replace("\\", "\\\\", $_POST['onderwerp'])); 
  
  if (mail($mail_ontv, $_POST['onderwerp'], $inhoud_mail, $headers))
  {
      
      $_SESSION['antiflood'] = time();
      
      echo '<h1>Het contactformulier is verzonden</h1>
      
      <p>Bedankt voor het invullen van het contactformulier. We zullen zo spoedig mogelijk contact met u opnemen.</p>';
  }
  else
  {
      echo '<h1>Het contactformulier is niet verzonden</h1>
      
      <p><b>Onze excuses.</b> Het contactformulier kon niet verzonden worden.</p>';
  }
}
?>
</div>
	<div id='footer'>Copyright by Flevoland Flowers </div>
</body>
<script> 
	$("#ppage").fadeIn(1);
	$("#header").fadeIn(1);
	$("#head2").fadeIn(1);
	$("#head3").fadeIn(1);
	$("#tabs").fadeIn(1);
	$("#footer").fadeIn(1);
  	$("#contact").addClass("cclick");
</script>
</html>
